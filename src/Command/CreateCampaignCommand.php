<?php


namespace App\Command;


use App\Entity\Account;
use App\Entity\Campaign;
use App\Services\Campaign\CampaignService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;

class CreateCampaignCommand extends Command
{
    protected static $defaultName = 'app:create-campaign';
    private $campaignService;
    private $entityManager;

    public function __construct(CampaignService $campaignService, EntityManagerInterface $entityManager)
    {
        $this->campaignService = $campaignService;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a default campaigns.')
            ->setHelp('This command allows you to create a default campaigns');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date =
        $clientA = $this->entityManager->getRepository(Account::class)->findAllClients()[0];
        $clientB = $this->entityManager->getRepository(Account::class)->findAllClients()[1];
        $campaign = new Campaign();
        $client = new Account();
        for ($i = 0; $i < 10; $i++) {
            $campaign = new Campaign();
            if ($i < 5) {
                $client = $clientA;
            } else {
                $client = $clientB;
            }
            $campaign
                ->setName('Test Campaign ' . $i)
                ->generateId()
                ->setStartDate(new \DateTime('2020-06-01'))
                ->setEndDate(new \DateTime('2020-06-21'));
            $this->campaignService->newCampaign($campaign,$client);
        }


        return 0;
    }
}