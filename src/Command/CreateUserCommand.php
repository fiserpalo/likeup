<?php


namespace App\Command;

use App\Services\Register\RegisterService;
use App\Services\Security\Credentials;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{

    protected static $defaultName = 'app:create-user';

    private $registerService;

    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a default users.')
            ->setHelp('This command allows you to create a default users');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $accounts = [
            new Credentials("admin@likeup.com", "jahoda123"),
            new Credentials("clientA@likeup.com", "jahoda123"),
            new Credentials("clientB@likeup.com", "jahoda123"),
            new Credentials("userA@likeup.com", "jahoda123"),
            new Credentials("userB@likeup.com", "jahoda123"),
        ];

        foreach ($accounts as $account) {

            if (substr_count($account->getEmail(),'admin' )> 0) {
                $this->registerService->register($account, "admin");
            } elseif (substr_count($account->getEmail(),'client' )> 0) {
                $this->registerService->register($account, "client");
            } elseif (substr_count($account->getEmail(),'user' )> 0) {
                $this->registerService->register($account, "user");
            }else{
                $output->writeln('!!!');

            }
        }

        return 0;
    }
}