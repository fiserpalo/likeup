<?php

namespace App\Repository;

use App\Entity\FilterOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FilterOption|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilterOption|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilterOption[]    findAll()
 * @method FilterOption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterOptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilterOption::class);
    }

    // /**
    //  * @return FilterOption[] Returns an array of FilterOption objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FilterOption
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
