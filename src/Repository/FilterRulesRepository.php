<?php

namespace App\Repository;

use App\Entity\FilterRules;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FilterRules|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilterRules|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilterRules[]    findAll()
 * @method FilterRules[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterRulesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilterRules::class);
    }

    // /**
    //  * @return FilterRules[] Returns an array of FilterRules objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FilterRules
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
