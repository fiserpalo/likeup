<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191201191245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE campaign (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', campaign_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', UNIQUE INDEX UNIQ_7FC45F1DF639F774 (campaign_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_group (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', group_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_option (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', option_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_rules (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', filter_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', filter_group_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', filter_type_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_A02B6AC3D395B25E (filter_id), INDEX IDX_A02B6AC3C33BDCE7 (filter_group_id), INDEX IDX_A02B6AC372DCFBF6 (filter_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_rules_filter_option (filter_rules_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', filter_option_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_CA245AEEC60697E4 (filter_rules_id), INDEX IDX_CA245AEE712FD182 (filter_option_id), PRIMARY KEY(filter_rules_id, filter_option_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_type (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', filter_group_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', type_name VARCHAR(255) NOT NULL, INDEX IDX_E4E43050C33BDCE7 (filter_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_type_filter_option (filter_type_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', filter_option_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_24F0D0E072DCFBF6 (filter_type_id), INDEX IDX_24F0D0E0712FD182 (filter_option_id), PRIMARY KEY(filter_type_id, filter_option_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE filter ADD CONSTRAINT FK_7FC45F1DF639F774 FOREIGN KEY (campaign_id) REFERENCES campaign (id)');
        $this->addSql('ALTER TABLE filter_rules ADD CONSTRAINT FK_A02B6AC3D395B25E FOREIGN KEY (filter_id) REFERENCES filter (id)');
        $this->addSql('ALTER TABLE filter_rules ADD CONSTRAINT FK_A02B6AC3C33BDCE7 FOREIGN KEY (filter_group_id) REFERENCES filter_group (id)');
        $this->addSql('ALTER TABLE filter_rules ADD CONSTRAINT FK_A02B6AC372DCFBF6 FOREIGN KEY (filter_type_id) REFERENCES filter_type (id)');
        $this->addSql('ALTER TABLE filter_rules_filter_option ADD CONSTRAINT FK_CA245AEEC60697E4 FOREIGN KEY (filter_rules_id) REFERENCES filter_rules (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_rules_filter_option ADD CONSTRAINT FK_CA245AEE712FD182 FOREIGN KEY (filter_option_id) REFERENCES filter_option (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_type ADD CONSTRAINT FK_E4E43050C33BDCE7 FOREIGN KEY (filter_group_id) REFERENCES filter_group (id)');
        $this->addSql('ALTER TABLE filter_type_filter_option ADD CONSTRAINT FK_24F0D0E072DCFBF6 FOREIGN KEY (filter_type_id) REFERENCES filter_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_type_filter_option ADD CONSTRAINT FK_24F0D0E0712FD182 FOREIGN KEY (filter_option_id) REFERENCES filter_option (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE filter DROP FOREIGN KEY FK_7FC45F1DF639F774');
        $this->addSql('ALTER TABLE filter_rules DROP FOREIGN KEY FK_A02B6AC3D395B25E');
        $this->addSql('ALTER TABLE filter_rules DROP FOREIGN KEY FK_A02B6AC3C33BDCE7');
        $this->addSql('ALTER TABLE filter_type DROP FOREIGN KEY FK_E4E43050C33BDCE7');
        $this->addSql('ALTER TABLE filter_rules_filter_option DROP FOREIGN KEY FK_CA245AEE712FD182');
        $this->addSql('ALTER TABLE filter_type_filter_option DROP FOREIGN KEY FK_24F0D0E0712FD182');
        $this->addSql('ALTER TABLE filter_rules_filter_option DROP FOREIGN KEY FK_CA245AEEC60697E4');
        $this->addSql('ALTER TABLE filter_rules DROP FOREIGN KEY FK_A02B6AC372DCFBF6');
        $this->addSql('ALTER TABLE filter_type_filter_option DROP FOREIGN KEY FK_24F0D0E072DCFBF6');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('DROP TABLE filter');
        $this->addSql('DROP TABLE filter_group');
        $this->addSql('DROP TABLE filter_option');
        $this->addSql('DROP TABLE filter_rules');
        $this->addSql('DROP TABLE filter_rules_filter_option');
        $this->addSql('DROP TABLE filter_type');
        $this->addSql('DROP TABLE filter_type_filter_option');
    }
}
