<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200108092354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE campaign ADD creator_id_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', ADD contributors_ids_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD type VARCHAR(255) DEFAULT NULL, ADD description VARCHAR(255) DEFAULT NULL, ADD target_count INT DEFAULT NULL, ADD current_count INT DEFAULT NULL, ADD status VARCHAR(255) DEFAULT NULL, ADD pic_url VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_1F1512DDF05788E9 FOREIGN KEY (creator_id_id) REFERENCES user_account (id)');
        $this->addSql('ALTER TABLE campaign ADD CONSTRAINT FK_1F1512DD80246DDC FOREIGN KEY (contributors_ids_id) REFERENCES user_account (id)');
        $this->addSql('CREATE INDEX IDX_1F1512DDF05788E9 ON campaign (creator_id_id)');
        $this->addSql('CREATE INDEX IDX_1F1512DD80246DDC ON campaign (contributors_ids_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DDF05788E9');
        $this->addSql('ALTER TABLE campaign DROP FOREIGN KEY FK_1F1512DD80246DDC');
        $this->addSql('DROP INDEX IDX_1F1512DDF05788E9 ON campaign');
        $this->addSql('DROP INDEX IDX_1F1512DD80246DDC ON campaign');
        $this->addSql('ALTER TABLE campaign DROP creator_id_id, DROP contributors_ids_id, DROP type, DROP description, DROP target_count, DROP current_count, DROP status, DROP pic_url');
    }
}
