<?php


namespace App\Services\Account;


use App\Entity\Account;
use Doctrine\ORM\EntityManagerInterface;

class AccountService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Security constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updateAccountProfile(Account $data,Account $account)
    {
        $account->setFirstName($data->getFirstName());
        $account->setLastName($data->getLastName());
        $account->setPhone($data->getPhone());
        $account->setSex($data->getSex());
        $this->entityManager->flush();
    }
}