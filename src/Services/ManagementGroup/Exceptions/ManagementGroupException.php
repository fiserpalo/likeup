<?php


namespace App\Services\ManagementGroup\Exceptions;


use App\Services\Exceptions\AppExceptions;
use App\Services\Register\Exceptions\RegisterException;

class ManagementGroupException extends AppExceptions
{

    public static function clientAlreadyHaveGroup(string $email)
    {
        return new ManagementGroupException("Client with email '{$email}' already have group ");
    }

}