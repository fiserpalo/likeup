<?php


namespace App\Services\ManagementGroup;

use App\Entity\Account;
use Doctrine\ORM\EntityManagerInterface;

class ManagementGroupService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Security constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addClientToGroup(\App\Entity\ManagementGroup $mGroup, string $accountId)
    {
        $client = $this->entityManager->getRepository(Account::class)->find($accountId)->getClient();
        $mGroup->addClient($client);
        $client->setManagementGroup($mGroup);
        $this->entityManager->flush();
    }

    public function removeClientFromGroup(\App\Entity\ManagementGroup $mGroup, string $accountId)
    {
        $client = $this->entityManager->getRepository(Account::class)->find($accountId)->getClient();
        $mGroup->removeClient($client);
        $client->setManagementGroup(null);
        $this->entityManager->flush();
    }


}