<?php


namespace App\Services\Campaign;


use App\Entity\Account;
use App\Entity\Campaign;
use App\Services\Campaign\Exceptions\CampaignException;
use Doctrine\ORM\EntityManagerInterface;

class CampaignService
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Security constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function newCampaign(Campaign $campaign, Account $account)
    {
        $campaign->generateId();
        $campaign->setCreator($account);
        $this->entityManager->persist($campaign);
        $this->entityManager->flush();
    }

    public function updateCampaign(Campaign $campaign, Campaign $data)
    {
        $campaign->setName($data->getName());
        $campaign->setStartDate($data->getStartDate());
        $campaign->setEndDate($data->getEndDate());
        $campaign->setCurrentCount($data->getCurrentCount());
        $campaign->setDescription($data->getDescription());
        $campaign->setPicUrl($data->getPicUrl());
        $campaign->setStatus($data->getStatus());
        $campaign->setTargetCount($data->getTargetCount());
        $campaign->setType($data->getType());

        $this->entityManager->flush();
    }

    public function removeCampaign(Campaign $campaign)
    {
        $this->entityManager->remove($campaign);
        $this->entityManager->flush();
    }

    public function getMyCampaigns(Account $user)
    {
        try {
            return $this->entityManager->getRepository(Campaign::class)->findBy(['creator' => $user]);
        } catch (\Exception $ex) {
            throw CampaignException::accountIsNotClient();
        }
    }

    public function getUsersCampaigns(Account $account)
    {
        try {
            return $this->entityManager->getRepository(Campaign::class)->findBy(['creator' => $account]);
        } catch (\Exception $ex) {
            throw CampaignException::accountIsNotClient();
        }
    }


}