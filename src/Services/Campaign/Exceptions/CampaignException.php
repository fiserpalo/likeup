<?php


namespace App\Services\Campaign\Exceptions;


use App\Services\Exceptions\AppExceptions;
use App\Services\Register\Exceptions\RegisterException;

class CampaignException extends AppExceptions
{

    public static function accountIsNotClient()
    {
        return new CampaignException("This account is not Client");
    }
}