<?php declare(strict_types=1);


namespace App\Services\Security\Exceptions;


use App\Services\Exceptions\AppExceptions;

class SecurityException extends AppExceptions
{

    public static function userNotFound(string $email)
    {
        return new SecurityException("User with email '{$email}' not found.'");
    }

    public static function incorrectPasswordProvided(string $email)
    {
        return new SecurityException("Invalid password provided for account '{$email}'");
    }
}