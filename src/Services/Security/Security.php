<?php declare(strict_types=1);

namespace App\Services\Security;

use App\Entity\Account;
use App\Services\Security\Exceptions\SecurityException;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class Security
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var JWTTokenManagerInterface
     */
    private $tokenManager;


    /**
     * Security constructor.
     * @param EntityManagerInterface $entityManager
     * @param JWTTokenManagerInterface $tokenManager
     */
    public function __construct(EntityManagerInterface $entityManager, JWTTokenManagerInterface $tokenManager)
    {
        $this->entityManager = $entityManager;
        $this->tokenManager = $tokenManager;
    }

    public function login(Credentials $credentials): array
    {
        $userRepository = $this->entityManager->getRepository(Account::class);

        $user = $userRepository->findOneBy(['email' => $credentials->getEmail()]);

        if ($user === null) {
            throw SecurityException::userNotFound($credentials->getEmail());
        }

        if (!password_verify($credentials->getPassword(), $user->getPassword())) {
            throw SecurityException::incorrectPasswordProvided($credentials->getEmail());
        }

        $token = $this->tokenManager->create($user);
        return [
            'token' => $token,
            'user' => $user
        ];
    }


}