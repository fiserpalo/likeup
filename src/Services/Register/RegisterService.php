<?php


namespace App\Services\Register;


use App\Entity\Account;
use App\Entity\Admin;
use App\Entity\Client;
use App\Entity\User;
use App\Services\Register\Exceptions\RegisterException;
use App\Services\Security\Credentials;
use Doctrine\ORM\EntityManagerInterface;

class RegisterService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Security constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function register(Credentials $credentials, string $type): ?Account
    {
        $accountRepository = $this->entityManager->getRepository(Account::class);

        $accout = $accountRepository->findOneBy(['email' => $credentials->getEmail()]);

        if ($accout !== null) {
            throw RegisterException::userAlreadyExist($credentials->getEmail());
        }

        $newAccout = new Account();
        switch ($type) {
            case "client":
                $newAccout->setClient(new Client());
                $newAccout->addRole('ROLE_CLIENT');
                break;
            case "admin":
                $newAccout->setAdmin(new Admin());
                $newAccout->addRole('ROLE_ADMIN');
                break;
            default:
            case "user":
                $newAccout->setUser(new User());
                break;
        }
        $newAccout->setPassword($credentials->getPassword())->hashPassword();
        $newAccout->setEmail($credentials->getEmail());
        $this->entityManager->persist($newAccout);
        $this->entityManager->flush();

        return $newAccout;

    }
}