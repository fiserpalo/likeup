<?php declare(strict_types=1);


namespace App\Services\Register\Exceptions;


use App\Services\Exceptions\AppExceptions;
use App\Services\Security\Credentials;

class RegisterException extends AppExceptions
{

    public static function userAlreadyExist(string $email)
    {
        return new RegisterException("Account with email '{$email}' already exist  ");
    }

    public function newCampaign()
    {

    }
}