<?php

namespace App\Controller;

use App\Entity\Account;
use App\Services\Account\AccountService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/user")
 */
class UserController extends AbstractController
{
    private $serializer;
    /**
     * @var AccountService
     */
    private $accountService;

    public function __construct(SerializerInterface $serializer, AccountService $accountService)
    {
        $this->serializer = $serializer;
        $this->accountService = $accountService;
    }

    /**
     * @Route("/", name="user", methods={"GET"})
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(Account::class)->findAllUsers();
        return new JsonResponse(
            $this->serializer->serialize(['users' => $users], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/profile", name="getAccount", methods={"GET"})
     */
    public function getProfile()
    {
        return new JsonResponse(
            $this->serializer->serialize(['user' => $this->getUser()], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/profile", name="updateAccountProfile", methods={"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function updateProfile(Request $request)
    {
        $data = $this->serializer->deserialize(
            $request->getContent(),
            Account::class,
            'json'
        );

        $this->accountService->updateAccountProfile($data, $this->getUser());

        return new JsonResponse(
            $this->serializer->serialize(['user' => $this->getUser()], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}

