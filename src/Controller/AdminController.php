<?php

namespace App\Controller;

use App\Entity\Account;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/", name="admin")
     */
    public function index()
    {
        $admins = $this->getDoctrine()->getRepository(Account::class)->findAllAdmins() ;
        return new JsonResponse(
            $this->serializer->serialize(['admins' => $admins], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
