<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\ManagementGroup;
use App\Services\ManagementGroup\ManagementGroupService;
use JMS\Serializer\SerializerInterface;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/management")
 */
class GroupManagementController extends AbstractController
{
    private $serializer;
    private $managementGroupService;

    public function __construct(SerializerInterface $serializer, ManagementGroupService $managementGroupService)
    {
        $this->serializer = $serializer;
        $this->managementGroupService = $managementGroupService;
    }

    /**
     * @Route("/", name="getAllGroups", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function getAllGroups(Request $request): Response
    {
        return new JsonResponse(
            $this->serializer->serialize(['managementGroup' => $this->getDoctrine()->getRepository(ManagementGroup::class)->findAll()], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }


    /**
     * @Route("/", name="addNewGroup", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function addNewGroup(Request $request): Response
    {
        $group = $this->serializer->deserialize(
            $request->getContent(),
            ManagementGroup::class,
            'json'
        );

        $group->generateId();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($group);
        $entityManager->flush();

        return new JsonResponse(
            $this->serializer->serialize($group, 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }

    /**
     * @Route("/{managementGroup}/account", name="addClientToGroup", methods={"POST"})
     * @param Request $request
     * @param ManagementGroup $managementGroup
     * @return Response
     * @throws JsonException
     */
    public function addClientToGroup(Request $request, ManagementGroup $managementGroup): Response
    {
        $this->managementGroupService->addClientToGroup($managementGroup, Json::decode($request->getContent())->account);

        return new JsonResponse(
            $this->serializer->serialize(['message' => 'success'], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/{managementGroup}/account", name="removeClientToGroup", methods={"DELETE"})
     * @param Request $request
     * @param ManagementGroup $managementGroup
     * @return Response
     * @throws JsonException
     */
    public function removeClientToGroup(Request $request, ManagementGroup $managementGroup): Response
    {
        $this->managementGroupService->removeClientFromGroup($managementGroup, Json::decode($request->getContent())->account);

        return new JsonResponse(
            $this->serializer->serialize(['message' => 'success'], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

}
