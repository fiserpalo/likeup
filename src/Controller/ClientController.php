<?php

namespace App\Controller;

use App\Entity\Account;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/client")
 */
class ClientController extends AbstractController
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/", name="client", methods={"GET"})
     */
    public function getAllClients()
    {
        $clients = $this->getDoctrine()->getRepository(Account::class)->findAllClients() ;
        return new JsonResponse(
            $this->serializer->serialize(['clients' => $clients], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
