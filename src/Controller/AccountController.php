<?php

namespace App\Controller;

use App\Entity\Account;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/account", name="account")
     */
    public function index()
    {
        $accounts = $this->getDoctrine()->getRepository(Account::class)->findAll() ;
        return new JsonResponse(
            $this->serializer->serialize(['accounts' => $accounts], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
