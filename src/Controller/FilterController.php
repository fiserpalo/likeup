<?php declare(strict_types=1);

namespace App\Controller;

use App\Repository\FilterRepository;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/filter")
 */
class FilterController extends AbstractController
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="filter")
     * @param FilterRepository $filterRepository
     * @return Response
     */
    public function index(FilterRepository $filterRepository): Response
    {
        return new JsonResponse(
            $this->serializer->serialize(['filters' => $filterRepository->findAll()], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}
