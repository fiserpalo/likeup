<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Campaign;
use App\Repository\CampaignRepository;
use App\Services\Campaign\CampaignService;
use App\Services\Campaign\Exceptions\CampaignException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/campaigns")
 */
class CampaignController extends AbstractController
{
    private $serializer;

    private $campaignService;

    public function __construct(SerializerInterface $serializer, CampaignService $campaignService)
    {
        $this->serializer = $serializer;
        $this->campaignService = $campaignService;
    }

    /**
     * @Route("/", name="campaign_index", methods={"GET"})
     * @param CampaignRepository $campaignRepository
     * @return Response
     */
    public function index(CampaignRepository $campaignRepository): Response
    {
        $ctx = SerializationContext::create()->setGroups(["allCampaigns"])->enableMaxDepthChecks();
        return new JsonResponse(
            $this->serializer->serialize(['campaigns' => $campaignRepository->findAll()], 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/my/", name="campaign_me", methods={"GET"})
     * @return Response
     */
    public function myCampaigns(): Response
    {
        $ctx = SerializationContext::create()->setGroups(["allCampaigns"])->enableMaxDepthChecks();
        return new JsonResponse(
            $this->serializer->serialize(['campaigns' => $this->getUser()->getCampaigns()], 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/{user}/campaigns", name="campaign_me", methods={"GET"})
     * @param CampaignRepository $campaignRepository
     * @param Account $user
     * @return Response
     */
    public function showUserCampaigns(CampaignRepository $campaignRepository, Account $user): Response
    {
        $ctx = SerializationContext::create()->setGroups(["allCampaigns"])->enableMaxDepthChecks();

        return new JsonResponse(
            $this->serializer->serialize(['campaigns' => $this->campaignService->getUsersCampaigns($user)], 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );
    }


    /**
     * @Route("/", name="campaign_new", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $campaign = $this->serializer->deserialize(
            $request->getContent(),
            Campaign::class,
            'json'
        );

        if (!$this->getUser()->getClient()) {
            throw CampaignException::accountIsNotClient();
        }

        $this->campaignService->newCampaign($campaign, $this->getUser());

        return new JsonResponse(
            $this->serializer->serialize($campaign, 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }

    /**
     * @Route("/my/", name="show_my_campaigns", methods={"GET"})
     * @param CampaignRepository $campaignRepository
     * @return Response
     */
    public function showMyCampaigns(CampaignRepository $campaignRepository): Response
    {
        $ctx = SerializationContext::create()->setGroups(["allCampaigns"])->enableMaxDepthChecks();


        return new JsonResponse(
            $this->serializer->serialize($this->campaignService->getMyCampaigns($this->getUser()), 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/{campaign}", name="campaign_show", methods={"GET"})
     * @param CampaignRepository $campaignRepository
     * @param Campaign $campaign
     * @return Response
     */
    public function show(CampaignRepository $campaignRepository, Campaign $campaign): Response
    {
        $ctx = SerializationContext::create()->setGroups(["allCampaigns"])->enableMaxDepthChecks();


        return new JsonResponse(
            $this->serializer->serialize($campaign, 'json', $ctx),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/{campaign}", name="campaign_edit", methods={"PUT"})
     * @param CampaignRepository $campaignRepository
     * @param Request $request
     * @param Campaign $campaign
     * @return Response
     */
    public function edit(CampaignRepository $campaignRepository, Request $request, Campaign $campaign): Response
    {
        $data = $this->serializer->deserialize(
            $request->getContent(),
            Campaign::class,
            'json'
        );

        $this->campaignService->updateCampaign($campaign, $data);

        return new JsonResponse(
            $this->serializer->serialize($campaign, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Campaign $campaign): Response
    {

        $this->campaignService->removeCampaign($campaign);

        return new JsonResponse(
            '',
            Response::HTTP_NO_CONTENT
        );
    }
}
