<?php declare(strict_types=1);

namespace App\Controller;

use App\Services\Exceptions\RegisterException;
use App\Services\Security\Credentials;
use App\Services\Security\Exceptions\SecurityException;
use App\Services\Security\Security;
use JMS\Serializer\SerializerInterface;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController extends AbstractController
{

    private $serializer;
    /**
     * @var Security
     */
    private $security;

    public function __construct(SerializerInterface $serializer, Security $security)
    {
        $this->serializer = $serializer;
        $this->security = $security;
    }

    /**
     * @Route("/login", name="app_login", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        try {
            $data = Json::decode($request->getContent(), 1);

            if (!isset($data['email'], $data['password'])) {
                return new JsonResponse(['error' => "Missing 'email' or 'password' field in request"], Response::HTTP_BAD_REQUEST);
            }

            $credentials = new Credentials($data['email'], $data['password']);
        } catch (JsonException $e) {
            return new JsonResponse(['error' => "Received data is not valid JSON."], Response::HTTP_BAD_REQUEST);
        }

        try {
            return new JsonResponse($this->security->login($credentials));
        } catch (SecurityException $e) {
            return new JsonResponse(['error' => 'Bad credentials'], Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }


}
