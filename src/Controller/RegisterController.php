<?php


namespace App\Controller;


use App\Entity\ManagementGroup;
use App\Services\Register\Exceptions\RegisterException;
use App\Services\Register\RegisterService;
use App\Services\Security\Credentials;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class RegisterController extends AbstractController
{

    private $registerService;

    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }

    /**
     * @Route("/register", name="register", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        try {
            $data = Json::decode($request->getContent(), JSON::FORCE_ARRAY);
            $credentials = new Credentials($data['email'], $data['password']);
        } catch (JsonException $e) {
            return new JsonResponse(['error' => "Received data is not valid JSON."], Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['email'], $data['password'])) {
            return new JsonResponse(['error' => "Missing 'email' or 'password' field in request"], Response::HTTP_BAD_REQUEST);
        }

        try {
            $this->registerService->register($credentials,$data['type']);
            return new JsonResponse(['message' => $data['type'] . " created"]);
        } catch (RegisterException $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }



}