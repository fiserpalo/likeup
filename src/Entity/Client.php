<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ManagementGroup", inversedBy="clients")
     */
    private $managementGroup;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account", inversedBy="client", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;


    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getManagementGroup(): ?ManagementGroup
    {
        return $this->managementGroup;
    }

    public function setManagementGroup(?ManagementGroup $managementGroup): self
    {
        $this->managementGroup = $managementGroup;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }


}
