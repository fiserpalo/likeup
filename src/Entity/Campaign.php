<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     * @Serializer\Groups({"allCampaigns"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Serializer\Groups({"allCampaigns"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d'>")
     * @Assert\Date()
     * @Serializer\Groups({"allCampaigns"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d'>")
     * @Assert\Date()
     * @Serializer\Groups({"allCampaigns"})
     */
    private $endDate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Filter", mappedBy="campaign", cascade={"persist", "remove"})
     */
    private $filter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $targetCount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $currentCount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $picUrl;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="campaigns")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Groups({"allCampaigns"})
     */
    private $creator;


    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function generateId(): self
    {
        $this->id = Uuid::uuid4();
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getFilter(): ?Filter
    {
        return $this->filter;
    }

    public function setFilter(Filter $filter): self
    {
        $this->filter = $filter;

        // set the owning side of the relation if necessary
        if ($filter->getCampaign() !== $this) {
            $filter->setCampaign($this);
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTargetCount(): ?int
    {
        return $this->targetCount;
    }

    public function setTargetCount(?int $targetCount): self
    {
        $this->targetCount = $targetCount;

        return $this;
    }

    public function getCurrentCount(): ?int
    {
        return $this->currentCount;
    }

    public function setCurrentCount(?int $currentCount): self
    {
        $this->currentCount = $currentCount;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPicUrl(): ?string
    {
        return $this->picUrl;
    }

    public function setPicUrl(?string $picUrl): self
    {
        $this->picUrl = $picUrl;

        return $this;
    }

    public function getCreator(): ?Account
    {
        return $this->creator;
    }

    public function setCreator(?Account $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

}
