<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterRepository")
 */
class Filter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Campaign", inversedBy="filter", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $campaign;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FilterRules", mappedBy="filter", orphanRemoval=true)
     */
    private $rules;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->rules = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getId()->toString();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Collection|FilterRules[]
     */
    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function addRule(FilterRules $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $this->rules[] = $rule;
            $rule->setFilter($this);
        }

        return $this;
    }

    public function removeRule(FilterRules $rule): self
    {
        if ($this->rules->contains($rule)) {
            $this->rules->removeElement($rule);
            // set the owning side to null (unless already changed)
            if ($rule->getFilter() === $this) {
                $rule->setFilter(null);
            }
        }

        return $this;
    }
}
