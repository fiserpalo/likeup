<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterTypeRepository")
 */
class FilterType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $TypeName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FilterRules", mappedBy="FilterType", orphanRemoval=true)
     */
    private $yes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FilterGroup", inversedBy="filterTypes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filterGroup;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FilterOption", inversedBy="filterTypes")
     */
    private $filterOptions;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->yes = new ArrayCollection();
        $this->filterOptions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTypeName();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getTypeName(): ?string
    {
        return $this->TypeName;
    }

    public function setTypeName(string $TypeName): self
    {
        $this->TypeName = $TypeName;

        return $this;
    }

    /**
     * @return Collection|FilterRules[]
     */
    public function getYes(): Collection
    {
        return $this->yes;
    }

    public function addYe(FilterRules $ye): self
    {
        if (!$this->yes->contains($ye)) {
            $this->yes[] = $ye;
            $ye->setFilterType($this);
        }

        return $this;
    }

    public function removeYe(FilterRules $ye): self
    {
        if ($this->yes->contains($ye)) {
            $this->yes->removeElement($ye);
            // set the owning side to null (unless already changed)
            if ($ye->getFilterType() === $this) {
                $ye->setFilterType(null);
            }
        }

        return $this;
    }

    public function getFilterGroup(): ?FilterGroup
    {
        return $this->filterGroup;
    }

    public function setFilterGroup(?FilterGroup $filterGroup): self
    {
        $this->filterGroup = $filterGroup;

        return $this;
    }

    /**
     * @return Collection|FilterOption[]
     */
    public function getFilterOptions(): Collection
    {
        return $this->filterOptions;
    }

    public function addFilterOption(FilterOption $filterOption): self
    {
        if (!$this->filterOptions->contains($filterOption)) {
            $this->filterOptions[] = $filterOption;
        }

        return $this;
    }

    public function removeFilterOption(FilterOption $filterOption): self
    {
        if ($this->filterOptions->contains($filterOption)) {
            $this->filterOptions->removeElement($filterOption);
        }

        return $this;
    }
}
