<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterOptionRepository")
 */
class FilterOption
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $OptionName;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FilterRules", mappedBy="filterOption")
     */
    private $filterRules;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FilterType", mappedBy="filterOptions")
     */
    private $filterTypes;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->filterRules = new ArrayCollection();
        $this->filterTypes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getOptionName();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getOptionName(): ?string
    {
        return $this->OptionName;
    }

    public function setOptionName(string $OptionName): self
    {
        $this->OptionName = $OptionName;

        return $this;
    }

    /**
     * @return Collection|FilterRules[]
     */
    public function getFilterRules(): Collection
    {
        return $this->filterRules;
    }

    public function addFilterRule(FilterRules $filterRule): self
    {
        if (!$this->filterRules->contains($filterRule)) {
            $this->filterRules[] = $filterRule;
            $filterRule->addFilterOption($this);
        }

        return $this;
    }

    public function removeFilterRule(FilterRules $filterRule): self
    {
        if ($this->filterRules->contains($filterRule)) {
            $this->filterRules->removeElement($filterRule);
            $filterRule->removeFilterOption($this);
        }

        return $this;
    }

    /**
     * @return Collection|FilterType[]
     */
    public function getFilterTypes(): Collection
    {
        return $this->filterTypes;
    }

    public function addFilterType(FilterType $filterType): self
    {
        if (!$this->filterTypes->contains($filterType)) {
            $this->filterTypes[] = $filterType;
            $filterType->addFilterOption($this);
        }

        return $this;
    }

    public function removeFilterType(FilterType $filterType): self
    {
        if ($this->filterTypes->contains($filterType)) {
            $this->filterTypes->removeElement($filterType);
            $filterType->removeFilterOption($this);
        }

        return $this;
    }
}
