<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterRulesRepository")
 */
class FilterRules
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Filter", inversedBy="Rules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FilterGroup", inversedBy="filterRules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filterGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FilterType", inversedBy="yes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filterType;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FilterOption", inversedBy="filterRules")
     */
    private $filterOption;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->filterOption = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getId()->toString();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getFilter(): ?Filter
    {
        return $this->filter;
    }

    public function setFilter(?Filter $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    public function getFilterGroup(): ?FilterGroup
    {
        return $this->filterGroup;
    }

    public function setFilterGroup(?FilterGroup $filterGroup): self
    {
        $this->filterGroup = $filterGroup;

        return $this;
    }

    public function getFilterType(): ?FilterType
    {
        return $this->filterType;
    }

    public function setFilterType(?FilterType $filterType): self
    {
        $this->filterType = $filterType;

        return $this;
    }

    /**
     * @return Collection|FilterOption[]
     */
    public function getFilterOption(): Collection
    {
        return $this->filterOption;
    }

    public function addFilterOption(FilterOption $filterOption): self
    {
        if (!$this->filterOption->contains($filterOption)) {
            $this->filterOption[] = $filterOption;
        }

        return $this;
    }

    public function removeFilterOption(FilterOption $filterOption): self
    {
        if ($this->filterOption->contains($filterOption)) {
            $this->filterOption->removeElement($filterOption);
        }

        return $this;
    }
}
