<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterGroupRepository")
 */
class FilterGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="uuid")
     * @Serializer\Type("uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $GroupName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FilterRules", mappedBy="FilterGroup", orphanRemoval=true)
     */
    private $filterRules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FilterType", mappedBy="filterGroup", orphanRemoval=true)
     */
    private $filterTypes;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
        $this->filterRules = new ArrayCollection();
        $this->filterTypes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getGroupName();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getGroupName(): ?string
    {
        return $this->GroupName;
    }

    public function setGroupName(string $GroupName): self
    {
        $this->GroupName = $GroupName;

        return $this;
    }

    /**
     * @return Collection|FilterRules[]
     */
    public function getFilterRules(): Collection
    {
        return $this->filterRules;
    }

    public function addFilterRule(FilterRules $filterRule): self
    {
        if (!$this->filterRules->contains($filterRule)) {
            $this->filterRules[] = $filterRule;
            $filterRule->setFilterGroup($this);
        }

        return $this;
    }

    public function removeFilterRule(FilterRules $filterRule): self
    {
        if ($this->filterRules->contains($filterRule)) {
            $this->filterRules->removeElement($filterRule);
            // set the owning side to null (unless already changed)
            if ($filterRule->getFilterGroup() === $this) {
                $filterRule->setFilterGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FilterType[]
     */
    public function getFilterTypes(): Collection
    {
        return $this->filterTypes;
    }

    public function addFilterType(FilterType $filterType): self
    {
        if (!$this->filterTypes->contains($filterType)) {
            $this->filterTypes[] = $filterType;
            $filterType->setFilterGroup($this);
        }

        return $this;
    }

    public function removeFilterType(FilterType $filterType): self
    {
        if ($this->filterTypes->contains($filterType)) {
            $this->filterTypes->removeElement($filterType);
            // set the owning side to null (unless already changed)
            if ($filterType->getFilterGroup() === $this) {
                $filterType->setFilterGroup(null);
            }
        }

        return $this;
    }

}
